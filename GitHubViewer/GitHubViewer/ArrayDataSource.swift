//
//  ArrayDataSource.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 20.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import UIKit

typealias ConfigureCellBlock = (cell: UITableViewCell, item: AnyObject) -> ()

class ArrayDataSource : NSObject, UITableViewDataSource {
    
    init(items: [AnyObject], cellIdentifier: String, configureCellBlock: ConfigureCellBlock) {
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.configureCellBlock = configureCellBlock
    }
    
    func itemAtIndexPath(indexPath: NSIndexPath) -> AnyObject {
        return items[indexPath.row]
    }
    
    @objc func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    @objc func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        let item = itemAtIndexPath(indexPath)
        
        configureCellBlock(cell: cell, item: item )
        
        return cell
    }
    
    private var items: [AnyObject]
    private var cellIdentifier: String
    private var configureCellBlock: ConfigureCellBlock
}
