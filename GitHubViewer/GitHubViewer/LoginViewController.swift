//
//  LoginViewController.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 19.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginTextFiled: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func signInTouch(sender: AnyObject) {
        
        guard let login = loginTextFiled.text, let password = passwordTextField.text else {
            return
        }
        
        showLoader(NSLocalizedString("loading.login", comment: ""))

        GitHubManager.sharedManager.login(login, password:password,
            success: { () -> () in
                self.performSegueWithIdentifier("showRepositories", sender: nil)
                self.hideLoader()
            
            }) { (error:Error) -> () in
                self.showError(error)
                self.hideLoader()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

