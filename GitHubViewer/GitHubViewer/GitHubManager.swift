//
//  GitHubManager.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 19.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import Foundation

import UAGithubEngine
import ObjectMapper

class GitHubManager {
    
    func login( login: String, password: String, success:()->(), failure:(error: Error)->() ) {
        githubEngine.username = login
        githubEngine.password = password
        
        githubEngine.userWithSuccess( { (response: AnyObject!) -> Void in
            success()
        }) { (error: NSError!) -> Void in

            if ( error.code == Int(CFNetworkErrors.CFURLErrorUserCancelledAuthentication.rawValue) )
            {
                failure(error:Error(message:NSLocalizedString("error.wrongLoginOrPassword", comment:"")))
                return
            }
            
            failure(error:Error(message:error.localizedDescription))
        }
    }
    
    func logout() {
        githubEngine.username = nil
        githubEngine.password = nil
    }
    
    func repositories(success:([Repository]?)->(), failure:(error:Error)->()) {
        
        githubEngine.repositoriesWithSuccess({ (responseObj:AnyObject!) -> Void in
            
            guard let reposRawData = responseObj as? [AnyObject] else {
                failure(error: Error(message:NSLocalizedString("error.unknownDataType", comment:"")))
                return
            }
            
            let repos = Mapper<Repository>().mapArray(reposRawData)
            
            success( repos )

            }) { (error:NSError!) -> Void in
                failure(error: Error(message: error.localizedDescription))
        }
    }
    
    func issues(state:Issue.State, repository:Repository, success:([Issue]?)->(), failure:(error:Error)->()) {
        
        guard let repositoryPath = repository.fullName else {
            return
        }
        
        switch state {
        
        case .Open:
            openIssues(repositoryPath, success: success, failure: failure)
        
        case .Closed:
            closedIssues(repositoryPath, success: success, failure: failure)
        
        default:
            failure(error: Error(message:NSLocalizedString("error.unsupportedIssueState", comment:"")))
        }
    }
    
    private func openIssues( repositoryPath:String, success:([Issue]?)->(), failure:(error:Error)->()) {
        
        githubEngine.openIssuesForRepository(repositoryPath, withParameters: nil,
            success:{ (responseObj:AnyObject!) -> Void in
            
                guard let reposRawData = responseObj as? [AnyObject] else {
                    failure(error: Error(message:NSLocalizedString("error.unknownDataType", comment:"")))
                    return
                }
                
                let issues = Mapper<Issue>().mapArray(reposRawData)
                
                success( issues )
            
            }) { (error:NSError!) -> Void in
                failure(error: Error(message: error.localizedDescription))
        }
    }
    
    private func closedIssues( repositoryPath:String, success:([Issue]?)->(), failure:(error:Error)->()) {
        
        githubEngine.closedIssuesForRepository(repositoryPath, withParameters: nil,
            success:{ (responseObj:AnyObject!) -> Void in
            
                guard let reposRawData = responseObj as? [AnyObject] else {
                    failure(error: Error(message:NSLocalizedString("error.unknownDataType", comment:"")))
                    return
                }
                
                let issues = Mapper<Issue>().mapArray(reposRawData)
                
                success( issues )
            
            }) { (error:NSError!) -> Void in
                failure(error: Error(message: error.localizedDescription))
        }
    }
    
    static let sharedManager = GitHubManager()
    
    private var githubEngine: UAGithubEngine
    
    private init() {
        githubEngine = UAGithubEngine()
    }
}
