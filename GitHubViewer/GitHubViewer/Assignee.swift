//
//  Assignee.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 20.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import Foundation
import ObjectMapper

class Assignee : Mappable {
    var login : String?
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        login <- map["login"];
    }
}
