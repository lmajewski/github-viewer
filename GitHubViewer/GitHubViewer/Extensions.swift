//
//  Extensions.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 20.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import UIKit
import SwiftSpinner

extension UIViewController {
    func showLoader(message: String) {
        SwiftSpinner.show(message)
    }
    
    func hideLoader() {
        SwiftSpinner.hide()
    }
    
    func showError(error: Error) {
        let alert = UIAlertView()
        alert.message = error.message
        alert.addButtonWithTitle(NSLocalizedString("alert.okButton", comment: ""))
        
        alert.show()
    }
}
