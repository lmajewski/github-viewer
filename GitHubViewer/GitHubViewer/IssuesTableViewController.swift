//
//  IssuesTableViewController.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 19.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import UIKit

class IssuesTableViewController: UITableViewController, UINavigationBarDelegate {
    
    let issueCellIdentifier = "IssueCell"
    
    enum SelectedState : Int {
        case Open = 0
        case Closed = 1
    }

    var repository : Repository?
    var selectedState : SelectedState?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = NSLocalizedString("viewController.title.issues", comment: "")
        
        selectedState = .Open
        
        let segmentController = UISegmentedControl(items:[NSLocalizedString("navBarButton.issueType.open", comment: ""), NSLocalizedString("navBarButton.issueType.closed", comment: "")])
        segmentController.selectedSegmentIndex = 0
        segmentController.addTarget(self, action: "statusSegmentChanged:", forControlEvents: .ValueChanged)
        segmentController.tintColor = UIColor(red:0.3, green: 0.3, blue: 0.3, alpha: 1.0)

        let barButton = UIBarButtonItem(customView: segmentController)
        
        self.navigationItem.rightBarButtonItem = barButton
        
        reloadData()
    }
    
    func statusSegmentChanged(sender:UISegmentedControl) {
        selectedState = SelectedState(rawValue: sender.selectedSegmentIndex)
        
        self.reloadData()
    }
    
    func reloadData() {
        
        guard let repository = repository, let selectedState = selectedState else {
            return
        }
        
        showLoader(NSLocalizedString("loading.downloadIssues", comment: ""))
        
        var state : Issue.State
        
        switch selectedState {
        
        case SelectedState.Open:
            state = Issue.State.Open
        
        case .Closed:
                state = Issue.State.Closed
        }
        
        GitHubManager.sharedManager.issues(state, repository: repository,
            success:{ (issues:[Issue]?) -> () in
            
                if ( issues != nil ) {
                    self.arrayDataSource = ArrayDataSource(items: issues!, cellIdentifier: self.issueCellIdentifier, configureCellBlock: self.configureCellBlock)
                    
                    self.tableView.dataSource = self.arrayDataSource
                } else {
                    self.tableView.dataSource = nil
                }
                
                self.tableView.reloadData()
                
                self.hideLoader()
            
            }) { (error) -> () in
                
                self.showError(error)
                self.hideLoader()
        }
    }

    // ArrayDataSource configurationCellBlock
    func configureCellBlock(cell: UITableViewCell, item: AnyObject) {
        
        guard let issue = item as? Issue else {
            return
        }
        
        cell.textLabel?.text = issue.title
        
        // Issue details not implemented
        //cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        if let assigneeLogin = issue.assignee?.login {
            cell.detailTextLabel?.text = NSLocalizedString("label.issueAssignee", comment:"") + assigneeLogin
        } else {
            cell.detailTextLabel?.text = NSLocalizedString("label.issueNotAssign", comment:"")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    }
    
    private var arrayDataSource : ArrayDataSource?
}
