//
//  Repository.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 19.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository : Mappable {
    var id : Int?
    var name : String?
    var fullName : String?
    var description : String?
    var repositoryPath : String?
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"];
        name <- map["name"];
        fullName <- map["full_name"];
        description <- map["description"];
        repositoryPath <- map["url"]
    }
    
}
