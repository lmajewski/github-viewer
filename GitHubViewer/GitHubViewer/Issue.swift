//
//  Issue.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 19.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import Foundation
import ObjectMapper

class Issue : Mappable {

    enum State : String {
        case Open = "open"
        case Closed = "close"
        case All = "all"
        case Unknown = "unknown"
    }
    
    var id : Int?
    var title : String?
    var state : State
    var assignee : Assignee?
    
    required init?(_ map: Map) {
        state = .Unknown
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        state <- map["state"]
        assignee <- map["assignee"]
    }
}
