//
//  RepositoriesTableViewController.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 19.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import UIKit

class RepositoriesTableViewController: UITableViewController {
    
    let repositoryCellIdentifier = "RepositoryCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("navBarButton.logout", comment:""), style: UIBarButtonItemStyle.Plain, target: self, action: "logoutTouch")
        
        reloadData()
    }
    
    func reloadData()
    {
        showLoader(NSLocalizedString("loading.downloadRepositories", comment: ""))
        
        GitHubManager.sharedManager.repositories({ (repositories:[Repository]?) -> () in

            if ( repositories != nil ) {
                self.arrayDataSource = ArrayDataSource(items: repositories!, cellIdentifier: self.repositoryCellIdentifier, configureCellBlock: self.configureCellBlock)
                
                self.tableView.dataSource = self.arrayDataSource
            } else {
                self.tableView.dataSource = nil
            }
            
            self.tableView.reloadData()
            
            self.hideLoader()
            
            }) { (error) -> () in
                
                self.showError(error)
                self.hideLoader()
        }
    }
    
    func logoutTouch() {
        GitHubManager.sharedManager.logout()
        self.performSegueWithIdentifier("showLogin", sender: nil)
    }
    
    // ArrayDataSource configurationCellBlock
    func configureCellBlock(cell: UITableViewCell, item: AnyObject) {
        
        guard let repository = item as? Repository else {
            return
        }
        
        cell.textLabel?.text = repository.name
        cell.detailTextLabel?.text = repository.description

        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showIssues" {
            guard let viewController = segue.destinationViewController as? IssuesTableViewController else {
                return
            }
            
            guard let indexPath = tableView.indexPathForSelectedRow else {
                return
            }
            
            guard let repository = arrayDataSource?.itemAtIndexPath(indexPath) as? Repository else {
                return
            }

            viewController.repository = repository
        }
    }
    
    private var arrayDataSource : ArrayDataSource?
}
