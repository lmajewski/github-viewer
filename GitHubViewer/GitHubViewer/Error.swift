//
//  Error.swift
//  GitHubViewer
//
//  Created by Łukasz Majewski on 19.12.2015.
//  Copyright © 2015 Sumito Development. All rights reserved.
//

import Foundation

class Error {
    var message : String?

    init() {
    }
    
    init(message:String) {
        self.message = message;
    }
}
